import { Component, OnInit } from '@angular/core';
import { PublicacionService } from 'src/app/services/publicacion.service';

@Component({
  selector: 'app-agregar-publi',
  templateUrl: './agregar-publi.component.html',
  styleUrls: ['./agregar-publi.component.css']
})
export class AgregarPubliComponent implements OnInit {

  constructor(private _postService:PublicacionService) { }

  ngOnInit(): void {
  }

  addPost(titulo:HTMLInputElement,descripcion:HTMLTextAreaElement){
 
    this._postService.addPost({
      titulo:titulo.value,
      fecha:new Date(),
      descripcion:descripcion.value
    })
    console.log(this._postService.obtenerPost());
    return false;
  }

}
