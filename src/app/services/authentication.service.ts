import { Injectable } from '@angular/core';
import { Auth, authState, createUserWithEmailAndPassword, updateProfile } from '@angular/fire/auth';
import { signInWithEmailAndPassword } from '@firebase/auth';
import { from, Observable, switchMap } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  currentUser$ = authState(this.auth);

  constructor(private auth : Auth) { }
  // Para subir a firebase
  login(email:string,password:string):Observable<any>{
   return from(signInWithEmailAndPassword(this.auth,email,password));
  }
  signUp(name:string,email: string, password: string) {
    return from(createUserWithEmailAndPassword(this.auth, email, password)).pipe(switchMap(({user})=> updateProfile(user,{displayName:name})));
  }
  logout(){
    return from(this.auth.signOut());
  }
}
