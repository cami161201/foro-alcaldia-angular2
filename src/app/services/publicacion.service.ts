import { Injectable } from '@angular/core';
import { IPublicacion } from '../models/publicacion.interface';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {
  post:IPublicacion[]=[];
  constructor() {
    this.post=[
      {titulo:'Italia',fecha:new Date(),descripcion:'Italia, país europeo con una larga costa mediterránea, influyó considerablemente en la cultura y la cocina occidental. Su capital, Roma, es hogar del Vaticano, de ruinas antiguas y de obras de arte emblemáticas.'},
      {titulo:'djkhf',fecha:new Date(),descripcion:'quiensabe q ddjf'}
    ];
  }

  obtenerPost(){
    return this.post;
  }

  addPost(publicacion:IPublicacion){
    this.post.push(publicacion);
    return false;
  }
}
